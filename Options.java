import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Options {
    public void addStudent(List<Student> list, Student student) {
        list.add(student);
    }

    public void display(List<Student> list) {
        int index = 1;
        for (Student student : list) {
            System.out.println("------ Student " + index + " Info ------");
            student.display();
            index++;
        }
    }

    public HashMap<String, Double> getPercentTypeStudent(List<Student> list) {
        // Với  A thì ta có value Double
        // A: 30%
        // B: 40%
        HashMap<String, Double> hashMap = new HashMap<>();
        int countA = 0;
        int countB = 0;
        int countC = 0;
        int countD = 0;
        for (Student o : list) {
            if (o.getType(o.getAvg()).equals("A")) {
                countA++;
            } else if (o.getType(o.getAvg()).equals("B")) {
                countB++;
            } else if (o.getType(o.getAvg()).equals("C")) {
                countC++;
            } else {
                countD++;
            }
        }
        // tổng số sinh viên bằng size của list
        int totalStudent = list.size();
        // tính phần trăm
        double percentA = (double) countA / totalStudent * 100;
        double percentB = (double) countB / totalStudent * 100;
        double percentC = (double) countC / totalStudent * 100;
        double percentD = (double) countD / totalStudent * 100;
        hashMap.put("A", percentA);
        hashMap.put("B", percentB);
        hashMap.put("C", percentC);
        hashMap.put("D", percentD);

        return hashMap;
    }
}

