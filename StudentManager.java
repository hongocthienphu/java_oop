import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class StudentManager {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        list.add(new Student("Nguyen Van A", "C01", 5, 5, 9.0));
        list.add(new Student("Nguyen Van B", "C02", 7.5, 8.5, 9.0));

        Options op = new Options();
        System.out.println("====== Management Student Program ======");
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Name: ");
            String name = sc.nextLine();
            System.out.print("Classes: ");
            String classes = sc.nextLine();
            System.out.print("Math: ");
            double math = Double.parseDouble(sc.nextLine());
            System.out.print("Physical: ");
            double physical = Double.parseDouble(sc.nextLine());
            System.out.print("Chemistry: ");
            double chemistry = Double.parseDouble(sc.nextLine());
            // Khởi tạo đối tượng student để add vào list
            Student student = new Student(name, classes, math, physical, chemistry);
            op.addStudent(list, student);
            System.out.print("Do you want to enter more student information?(Y/N): ");
            String choice = sc.nextLine();
            if(choice.equalsIgnoreCase("N")){
                break;
            }
        }
        op.display(list);
        HashMap<String, Double> mapStudent = op.getPercentTypeStudent(list);
        mapStudent.forEach((key, value) -> System.out.println(key + ": " + value + "%"));
    }
}
